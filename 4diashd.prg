/* Copyright - 2011, Francisco Manzano Magaña

	Fecha de comienzo: 6 de Julio de 2011
	Fecha de finalización: 10 de Julio de 2011
	
	Proyecto: "4 días cubierto de mierda" / "Four days shit-covered"
	
	Descripción del proyecto: Juego realizado en 4 dias para el Crap Game Compo 2011: Coding for food.
	
	****************************************
	Revisión del proyecto: Adaptación a PC totalmente.

Proyecto:	"4 Días HD"

Fecha de comienzo: 19 de Septiembre de 2011

Fecha de finalización:  Estimado <Marzo 2024>

Descripción del proyecto: Adaptación del juego "4 días cubierto de mierda" a alta resolucion. 1280x720 resolución y 30fps.

Objetivo: Realizar la primera prueba de juego q puede llegar a ser vendible, aunq no lo sea. Aumentando la calidad en lo posible. Variación de la perspectiva. Más variedad de enemigos. Más armas. Obstaculos destruibles. 

Juegos donde se inspira: 
			
			- Diablo.
			- World of Warcraft

Se estima la posibilidad de desarrollar el juego desde cero en Unity3D

*/

import "mod_text";
import "mod_video";
import "mod_key";
import "mod_math";
import "mod_draw";
import "mod_rand";
import "mod_map";
import "mod_proc";
import "mod_screen";
import "mod_scroll";
import "mod_grproc";
import "mod_string";
import "mod_sound";
import "mod_joy";
import "mod_say";

Const
	c_version=0.05;
	alto_pantalla=600;
	ancho_pantalla=1060;
	DEBUG_MODE = 1;			// Indica si se esta en desarrollo o en producción (1=True, 0=False)
end

include "code/general_defs.prg"
include "code/ScreenPad/ScreenPad.prg"
include "code/kontrol.prg"
include "code/klsystem.prg"
include "code/isotile.prg"
include "code/player.prg"

Global
	
	int ID_fpg_adornos;
	int ID_fpg_suelos;

	int suelomap;
	
	_colorLevel nivelito;
end

include "code/objetos.prg"


Process Main()

Begin
	

	if(os_id == OS_ANDROID)	
		scale_resolution=08000480;
	end
	set_mode(ancho_pantalla,alto_pantalla,32);
	
	set_fps(30,1);	
	write_var(0,10,10,0,fps);


	nivelito.playerID = PLYR_jugador();

	CL_loadFullColorLevel("grande", &nivelito);

	if(os_id == OS_ANDROID)
		screenPadActive = true;
	else
		screenPadActive = false;
		if(joy_number()<>0)
			joy_select(0);
			joy_activo=true;
		end	
	end

	if(screenPadActive)
		SP_ScreenPad();
	end

	ID_fpg_adornos = load_fpg("fpg/adornos.fpg");

	OBJ_EstaticoRef( 550, 220, 0,"tiesto");
	OBJ_EstaticoRef(650, 220, 0,"tiesto");
	OBJ_EstaticoRef(750, 220, 0,"tiesto");
	OBJ_EstaticoRef(850, 220, 0,"tiesto");

	CL_assetController(nivelito.playerID, &nivelito);

	loop
		
		if(check_boton(t_cancel)) exit(); end
		frame;
		
	end
end