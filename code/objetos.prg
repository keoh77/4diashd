process OBJ_Estatico (xi, yi, zi, file, graph)

begin
	ctype = C_SCROLL;
	cnumber = c_0;
	pinta();
	signal(id, s_freeze);
	frame;
end

process OBJ_EstaticoRef(xi, yi, zi, string referencia)

begin

	switch(referencia)
		case "tiesto":
			file = ID_fpg_adornos;
			graph = 1;
		end

		default:
			file = ID_fpg_adornos;
			graph = 1;
		end
	end

	ctype = C_SCROLL;
	cnumber = c_0;

	pinta();
	signal(id, s_freeze);
	frame;	
end