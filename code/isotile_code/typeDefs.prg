/*
	Definiciones de datos para el motor de tileado

	Motor Tileado Isometrico de KeoH 1.0

*/

type itileObjeto
	
	int referencia;		// referencia del objeto para despues reconstruirlo
	int tX, tY;			// coordenadas dentro del tileado
	int capa = 1;		// capa a la que pertenece el objeto (0 en la capa fondo, 1 en la normal, 2 en el frontal)
end

type itilemap

	string mapName = "NoMapName";		// Nombre del mapa/nivel
	string mapAuthor = "NoAuthorName";	// Nombre del autor
	
	string gameName = "NoGameName";		// Juego para el q es el nivel
	
	int anchura = 20;				// Altura en numero de tiles por defecto
	int altura = 20;				// Anchura en numero de tiles por defecto
	
	string fpgGraficos = "";		// Fichero FPG que contiene los graficos del tileado
	string fpgDurezas = "";			// Fichero FPG que contiene las durezas
	string fpgGraficosFondo = "";
	string fpgGraficosFrontal = "";
	
	int idFpgGraficos, idFpgDurezas, idFpgGraficosFondo, idFpgGraficosFrontal;
	
	int anchuraTiles = 32; 			//Anchura y altura standart de los tiles.
	int alturaTiles = 32;
	
	int pointer tileRef;			// referencia al tile que debe ponerse
	int pointer tileDureza;			//dureza que tendrá el tile
	
	int anchuraTilesFondo = 32;		// Anchura y altura de los tiles de fondo que sirven
	int alturaTilesFondo = 32;		// como adorno.
	
	int tileFondoRef;					//referencia del tile de fondo
	int tileFondoPos; 				// posicion de tile normal bajo el que se coloca el tile de fondo
	
	int anchuraTilesFrontal = 32;	// Anchura y aluta del los tiles que salen en primer plano
	int alturaTilesFrontal = 32;	// sirven como adorno (lo suyo es que sean grandes, y sean 
									// como los arboles de Paper.

	int tileFrontalRef;				// referencia del tile frontal
	int tileFrontalPos;				// posicion de tile normal sobre el que se coloca el tile frontal
	
	itileObjeto pointer tObj;
	
	
end

type itilescroll
	
	int tRegion;
	itilemap pointer tmap;



end