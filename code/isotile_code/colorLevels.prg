/*
	Modulo destinado a la carga de niveles del juego interpretando imagenes.

	Los niveles se cargaran con una o mas imagenes que representaran los objetos y los suelos a poner.

*/

import "mod_map";
import "mod_say";
import "mod_mem";
import "mod_dir";
import "mod_screen";
import "mod_scroll";

#define ANCHO	1
#define ALTO	2

type _tileAsset
	int idTile;
	int t_ancho=0;
	int t_alto=0;

end

type _itile
	//int posicion[1];
	int idProcesoTile[100][100];
end

type _colorLevel

	int patronmap;		// Mapa patron con los colores.
	int tilemap;		// Mapa que describe los tiles a mostrar
	int durezamap;		// Mapa que describe las durezas
	int objetomap;		// Mapa que dice donde estan los objetos fijos

	int* colores;		// Colores del mapa. Minimo tiene dos. Blanco y negro
	int numColores;

	int filasDeTiles;	// Numero de columnas y filas de tileas que hay
	int columnasDeTiles;
	_tileAsset* graphTile;
	_itile tile;
	int playerID;

end

global
	int gameRegion;
end

/*
	CL_loadFullColorLevel() :	Es la funcion que hace todas las gestiones para la carga del nivel
*/

function CL_loadFullColorLevel(string nombreNivel, _colorLevel pointer nivel)

begin

	CL_loadColorLevel(nombreNivel, [nivel]);	// Cargamos los graficos que describen en nivel
	CL_checkColorLevelOK([nivel]);				// Comprobamos que esten todos los graficos y de tamaños correctos
	CL_loadPatron([nivel], CL_countPatronColors([nivel])); // Cargamos en numero de colores del patron y esos colores
	CL_loadTilesAssets(nombreNivel,[nivel]);	//Carga los graficos de los tiles del directorio del nivel
	CL_createAssignTiles([nivel]);				//Coloca los tiles en pantalla.
	CL_ScrollRegion([nivel]);
	// Andes de descargar tiene que gestionarse la carga de las durezas y objetos (pa despues)
	//CL_unloadColorLevel([nivel]);				// Descargamos esos graficos de descripcion que ya no sirven

end

function CL_ScrollRegion(_colorLevel pointer nivel)

private
	int scrollsize[1];
end

begin
	scrollsize[0] = [nivel].columnasDeTiles * [nivel].graphTile.t_ancho;
	scrollsize[1] = [nivel].filasDeTiles * [nivel].graphTile.t_alto;
	gameRegion = define_region(1, 0, 0, ancho_pantalla, alto_pantalla*0.8);
	start_scroll(0, 0, map_new(1,1,8),map_new(1,1,8),1,3);

end
/*
	CL_createAssignTiles(): Pone los tiles en pantalla en funcion de los datos de la estructura de datos _colorLevel.
*/
function CL_createAssignTiles(_colorLevel pointer nivel)

private
	int colorcillo;
	int i;
	
end

begin
	x=y=0;
	[nivel].filasDeTiles = CL_getSize(0, [nivel].tilemap, ALTO);
	[nivel].columnasDeTiles = CL_getSize(0, [nivel].tilemap, ANCHO);	

	//[nivel].tile = calloc([nivel].filasDeTiles*[nivel].columnasDeTiles, sizeof(_itile));

	from y=0 to ([nivel].filasDeTiles-1) step 1;
		from x=0 to ([nivel].columnasDeTiles-1) step 1;
			colorcillo = map_get_pixel(0, [nivel].tilemap,x,y);
			from i=0 to [nivel].numColores step 1;
				if(colorcillo == [nivel].colores[i])
					[nivel].tile.idProcesoTile[x][y] = Tileado_Tile(x*[nivel].graphTile[i].t_ancho,y*[nivel].graphTile[i].t_alto,[nivel].graphTile[i].idTile);
					//say("Tile en posicion "+x+" "+y+" con ID: "+[nivel].tile[contador].idProcesoTile[x][y]);
					//say([nivel].graphTile[i].t_alto);
					
				end
			end
		end
	end
end

/*
	CL_assetController(): 	Megaproceso que controla que tiles hay q destruir y crear en el escenario
*/
process CL_assetController(int IDPlayer, _colorLevel pointer nivel)

private
	int actualPos[1];
	int anteriorPos[1];
	int numAncho, numAlto;
	int topeStructX = 99;
	int topeStructY = 99;
	int i=0;
	int j=0;
	int w, colorcillo;
	int principioy, finaly, principiox, finalx;
end

begin


	//numAncho = ancho_pantalla/64;
	numAncho = 5; //test
	numAlto = 5; //test
	//numAlto = alto_pantalla/32;
	actualPos[0] = (scroll[0].camera.x)/64;
	actualPos[1] = (scroll[0].camera.y)/32;
	anteriorPos[0] = actualPos[0];
	anteriorPos[1] = actualPos[1];

	loop
		actualPos[0] = (scroll[0].camera.x)/64;
		actualPos[1] = (scroll[0].camera.y)/32;

		if(actualPos[0] <> anteriorPos[0] or actualPos[1] <> anteriorPos[1])

			from j=0 to topeStructY;
				from i=0 to topeStructX;
					
					if(i>(actualPos[0]+numAncho) or i<(actualPos[0]-numAncho))
						if(actualPos[0]-numAncho<0)
							//x negativas
							if(exists([nivel].tile.idProcesoTile[i][j]))
								signal([nivel].tile.idProcesoTile[i][j],s_wakeup);
								//say("Aqui llega "+i+" "+j);
							end
						end
						if(actualPos[0]+numAncho>topeStructX)
							// x mayor q 99
							if(exists([nivel].tile.idProcesoTile[i][j]))
								signal([nivel].tile.idProcesoTile[i][j],s_wakeup);
								//say("Aqui llega "+i+" "+j);
							end
						end
						if(exists([nivel].tile.idProcesoTile[i][j]))
							signal([nivel].tile.idProcesoTile[i][j],s_wakeup);
							//say("Aqui llega "+i+" "+j);
						end
					end

					if(j>(actualPos[1]+numAlto) or j<(actualPos[1]-numAlto))
						if(actualPos[1]-numAlto<0)
							//x negativas
							if(exists([nivel].tile.idProcesoTile[i][j]))
								signal([nivel].tile.idProcesoTile[i][j],s_wakeup);
								//say("Aqui llega "+i+" "+j);
							end
						end
						if(actualPos[1]+numAlto>topeStructY)
							// x mayor q 99
							if(exists([nivel].tile.idProcesoTile[i][j]))
								signal([nivel].tile.idProcesoTile[i][j],s_wakeup);
								//say("Aqui llega "+i+" "+j);
							end
						end
						if(exists([nivel].tile.idProcesoTile[i][j]))
							signal([nivel].tile.idProcesoTile[i][j],s_wakeup);
							//say("Aqui llega "+i+" "+j);
						end
					end
					
				end
			end
			// Hasta aqui la destruccion de tiles lejanos ... ahora la construccion de nuevos tiles mas cercanos.

			principioy = actualPos[1]-numAlto;
			if(principioy<0) principioy=0; end
			finaly = actualPos[1]+numAlto;
			if(finaly>topeStructY) finaly=topeStructY; end
			principiox = actualPos[0]-numAncho;
			if(principiox<0) principiox=0; end
			finalx = actualPos[0]+numAncho;
			if(finalx>topeStructX) finalx=topeStructX; end


			if(actualPos[0]>anteriorPos[0])
				//say("nivel 1");
				from j=principioy to finaly;
					//say("nivel 2");
					colorcillo = map_get_pixel(0, [nivel].tilemap,finalx,j);
					from w=0 to [nivel].numColores step 1;
						//say("nivel 3");
						if(colorcillo == [nivel].colores[w])
							[nivel].tile.idProcesoTile[finalx][j] = Tileado_Tile(finalx*[nivel].graphTile[w].t_ancho,j*[nivel].graphTile[w].t_alto,[nivel].graphTile[w].idTile);
							//say("tile creado en "+i+" "+j);
						end
					end

				end
			end

			if(actualPos[0]<anteriorPos[0])
				//say("nivel 1");
				from j=principioy to finaly;
					//say("nivel 2");
					colorcillo = map_get_pixel(0, [nivel].tilemap,principiox,j);
					from w=0 to [nivel].numColores step 1;
						//say("nivel 3");
						if(colorcillo == [nivel].colores[w])
							[nivel].tile.idProcesoTile[principiox][j] = Tileado_Tile(principiox*[nivel].graphTile[w].t_ancho,j*[nivel].graphTile[w].t_alto,[nivel].graphTile[w].idTile);
							//say("tile creado en "+i+" "+j);
						end
					end

				end
			end

			if(actualPos[1]>anteriorPos[1])
				//say("nivel 1");
				from i=principiox to finalx;
					//say("nivel 2");
					colorcillo = map_get_pixel(0, [nivel].tilemap,i,finaly);
					from w=0 to [nivel].numColores step 1;
						//say("nivel 3");
						if(colorcillo == [nivel].colores[w])
							[nivel].tile.idProcesoTile[i][finaly] = Tileado_Tile(i*[nivel].graphTile[w].t_ancho,finaly*[nivel].graphTile[w].t_alto,[nivel].graphTile[w].idTile);
							//say("tile creado en "+i+" "+j);
						end
					end

				end
			end

			if(actualPos[1]<anteriorPos[1])
				//say("nivel 1");
				from i=principiox to finalx;
					//say("nivel 2");
					colorcillo = map_get_pixel(0, [nivel].tilemap,i,principioy);
					from w=0 to [nivel].numColores step 1;
						//say("nivel 3");
						if(colorcillo == [nivel].colores[w])
							[nivel].tile.idProcesoTile[i][principioy] = Tileado_Tile(i*[nivel].graphTile[w].t_ancho,principioy*[nivel].graphTile[w].t_alto,[nivel].graphTile[w].idTile);
							//say("tile creado en "+i+" "+j);
						end
					end

				end
			end

		end
		



		frame;
		anteriorPos[0] = actualPos[0];
		anteriorPos[1] = actualPos[1];
	end
end

/*
	CL_loadTilesAssets():
*/
function CL_loadTilesAssets(string nombreNivel, _colorLevel pointer nivel)

private
	string s="";
	int i;
end

begin
	
	[nivel].graphTile = calloc([nivel].numColores, sizeof(_tileAsset));

	from i=2 to ([nivel].numColores-1) step 1;
		s = glob("colorlevels/"+nombreNivel+"/tile"+(i-1)+".png");

		if(s=="") break; end
		//say(s);
		[nivel].graphTile[i].idTile = png_load("colorlevels/"+nombreNivel+"/"+s);
		// Ahora pondremos el centro de control en la esquina superior izquierda.
		[nivel].graphTile[i].t_ancho = CL_getSize(0, [nivel].graphTile[i].idTile, ANCHO);
		[nivel].graphTile[i].t_alto = CL_getSize(0, [nivel].graphTile[i].idTile, ALTO);
		graphic_set(0, [nivel].graphTile[i].idTile, G_X_CENTER, 0);
		graphic_set(0, [nivel].graphTile[i].idTile, G_Y_CENTER, 0);
		//say("Archivo: "+s+" ID: "+[nivel].graphTile[i].idTile+" De tamaño: "+[nivel].graphTile[i].t_ancho+"x"+[nivel].graphTile[i].t_alto+" Centro: "+graphic_info(0,[nivel].graphTile[i].idTile,G_X_CENTER)+","+graphic_info(0,[nivel].graphTile[i].idTile,G_Y_CENTER));
	end

end

function CL_loadColorLevel(string nombreNivel, _colorLevel pointer nivel)

begin
	[nivel].patronmap = png_load("colorlevels/"+nombreNivel+"/"+nombreNivel+"_patronmap.png");
	[nivel].tilemap = png_load("colorlevels/"+nombreNivel+"/"+nombreNivel+"_tilemap.png");
	[nivel].durezamap = png_load("colorlevels/"+nombreNivel+"/"+nombreNivel+"_durezamap.png");
	[nivel].objetomap = png_load("colorlevels/"+nombreNivel+"/"+nombreNivel+"_objetomap.png");
end

function CL_unloadColorLevel(_colorLevel pointer nivel)

begin
	unload_map(0, [nivel].patronmap);
	unload_map(0, [nivel].tilemap);
	unload_map(0, [nivel].durezamap);
	unload_map(0, [nivel].objetomap);
end

function CL_getSize(int fpg, int grafico, dimension)

begin
	switch(dimension)

		case ANCHO:
			return graphic_info(fpg, grafico,0);
		end

		case ALTO:
			return graphic_info(fpg, grafico,1);
		end
		default:
			return false;
		end
	end
end



function CL_checkColorLevelOK(_colorLevel pointer nivel)

begin
	if([nivel].tilemap and [nivel].durezamap and [nivel].objetomap)
		
		if(CL_getSize(0,[nivel].tilemap,ANCHO) == CL_getSize(0,[nivel].durezamap,ANCHO) and CL_getSize(0,[nivel].durezamap,ANCHO) == CL_getSize(0,[nivel].objetomap,ANCHO))
			
			if(CL_getSize(0,[nivel].tilemap,ALTO) == CL_getSize(0,[nivel].durezamap,ALTO) and CL_getSize(0,[nivel].durezamap,ALTO) == CL_getSize(0,[nivel].objetomap,ALTO))
				return true;
			else
				say("No coinciden las dimensiones de los mapas");
				return false;
			end
		else
			say("¡¡Los tamaños de los mapas no son iguales!!");
			return false;
		end	
	else
		say("¡¡Error en la carga de los archivos de ColorLevels!!");
		return false;
	end
end

function CL_loadPatron(_colorLevel pointer mapa, int numColores)

private
	int anchom, altom;
	int contador = 0;
	int myrgb[2];
end

begin
	x = y = 0;
	anchom = CL_getSize(0, [mapa].patronmap, ANCHO);
	altom = CL_getSize(0, [mapa].patronmap, ALTO);

	[mapa].colores = calloc(numColores,sizeof(int));

	from y=0 to (altom-1) Step 1;
		from x=0 to (anchom-1) Step 1;
			[mapa].colores[contador] = map_get_pixel(0, [mapa].patronmap, x, y);
			contador++;
			rgb_get([mapa].colores[contador-1], &myrgb[0], &myrgb[1], &myrgb[2]);
			//say([mapa].colores[contador-1]+" :"+myrgb[0]+","+myrgb[1]+","+myrgb[2]);
			if(contador > (numColores-1))
				return;
			end
		end
	end
end

/*
	CL_countPatronColors(): Detecta el numero de colores que hay en el patron.
*/

function int CL_countPatronColors(_colorLevel pointer mapa)

private
	int anchom, altom;
	int contador = 0;
	int colorbase, color2;
end

begin
	x = y = 0;
	anchom = CL_getSize(0, [mapa].patronmap, ANCHO);
	altom = CL_getSize(0, [mapa].patronmap, ALTO);

	colorbase = map_get_pixel(0, [mapa].patronmap, 0,0);

	while(y<altom)
		while(x<anchom)
			
			if(x == 0 and y == 0)
				contador++;
				x++;
				continue;
			end

			color2 = map_get_pixel(0, [mapa].patronmap, x, y);

			if(color2 == colorbase)
				x = anchom+1;
				y = altom + 1;
				break;
			else
				contador++;
			end
			x++;
		end
		x=0;
		y++;
	end
	[mapa].numColores = contador;
	return contador;
end
