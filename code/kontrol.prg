/*
	Funciones para el control de personajes y menus

*/

Global
	
	// Teclas de movimiento

	int t_der=_right; t_izq=_left; t_up=_up; t_down=_down; 
	int t_cancel=_esc;

	//int t_uso_1=_q; t_uso_2=_w; t_uso_3=_e; t_uso_4=_r; t_uso_5=_t; t_uso_6=_a; t_uso_7=_s; t_uso_8=_d; t_uso_9=_f;
	int t_uso[9] = (0, _q, _w, _e, _r, _t, _a, _s, _d, _f); 

	byte joy_activo=false; // indica si hay joystick conectado

end


include "code/kontrol/kontrol_def.prg"

include "code/kontrol/kontrol.prg"