
// Definicion de tipos de datos
include "code/isotile_code/typeDefs.prg"

// Definicion de datos y funciones de la carga de niveles por mapas de colores
include "code/isotile_code/colorLevels.prg"

//Funciones para carga y guardado de niveles
//include "code/isotile_code/load_save_tiles.prg"

// Funciones para la colocar el tileado en el juego
include "code/isotile_code/tileado.prg"



/*
	pinta(): Se encarga de poner las coordenadas internas a coordenadas de pantalla ... aqui esta toda la magia :P
*/
function pinta()

begin
	father.x=father.xi;
	father.y=father.yi-father.zi;
	father.z=alto_pantalla-father.y;
end