/*
	La logica del personaje va aqui

*/

import "mod_map";
import "mod_say";
import "mod_mem";
import "mod_dir";

process PLYR_mueve_prota()

begin
	while(exists(father))
		if(check_pad()<>0 and father.estado<>2)
			father.direccion=check_pad();
			father.estado=1;
		end
		if(check_pad()==0 and father.estado<>2)
			father.estado=0;
		end
		if(check_boton(t_uso[2]) and father.estado<>2)
			father.estado=2;
		end

		frame;
	end
end

process PLYR_jugador()

private
	int i,j;
	int px,py;
end

begin
	
	ctype = C_SCROLL;
	cnumber = c_0;
	
	xi=ancho_pantalla/2;
	yi=alto_pantalla*0.8;
	zi=0;
	
	size=100;

	from i=0 to 7 step 1;
		animaciones.Idle[i].numFrames = 1;
		animaciones.Andar[i].numFrames = 3;
	end

	from i=0 to 7 step 1;
		animaciones.Idle[i].aframe = calloc(1, sizeof(int));
		animaciones.Andar[i].aframe = calloc(3, sizeof(int));
	end

	from i=0 to 7 step 1;
		from j=0 to (animaciones.Andar[i].numFrames - 1) step 1;
			animaciones.Andar[i].aframe[j] = png_load("images/player/P_walk_0"+(i+1)+"_0"+(j+1)+".png");
			px = graphic_info(0, animaciones.Andar[i].aframe[j], 0);
			py = graphic_info(0, animaciones.Andar[i].aframe[j], 1);
			point_set(0, animaciones.Andar[i].aframe[j], 0, px/2, py-1);
		end
		animaciones.Idle[i].aframe[0] = png_load("images/player/P_walk_0"+(i+1)+"_01.png");
		px = graphic_info(0, animaciones.Idle[i].aframe[0], 0);
		py = graphic_info(0, animaciones.Idle[i].aframe[0], 1);
		point_set(0, animaciones.Idle[i].aframe[0], 0, px/2, py-1);
	end

	scroll[0].camera = id;
	PLYR_mueve_prota();
	loop

		while(estado == 0)	//Parado

			graph = animaciones.Idle[direccion-1].aframe[0];
			pinta();
			frame;
		end

		while(estado == 1)
			if(lastFrame > (animaciones.Andar[direccion-1].numFrames-1))
				lastFrame = 0;
			end

			graph = animaciones.Andar[direccion-1].aframe[lastFrame];
			lastFrame++;
			PLYR_avanza_direccion();
			pinta();
			frame;
		end
	end

end

function PLYR_salto()  // eleva al personaje q salta

begin
	father.zi+=fuerza_salto;
end

function PLYR_caigo() // desciende al personaje

begin
	father.zi-=gravedad;
end

function PLYR_check_suelo() // comprueba la distancia al suelo, si esta cerca (+- 3 pixeles) retorna si, y el pj aterriza

private
	altura_suelo=0;
	tolerancia=3;
end

begin
	if(abs(father.zi-altura_suelo)<tolerancia) 
		return true; 
	end
	if(abs(father.zi-altura_suelo)>tolerancia) 
		return false;	
	end	
end

function PLYR_avanza_direccion()

private
	float angulo = 26565.05118;
end

begin
	//father.ang_avance=45000*(father.direccion-1);	



	switch (father.direccion)
		case 1:
			father.ang_avance= 0;
		end
		case 2:
			father.ang_avance= angulo;
		end
		case 3:
			father.ang_avance= 90000;
		end
		case 4:
			father.ang_avance= 180000-angulo;
		end
		case 5:
			father.ang_avance= 180000;
		end
		case 6:
			father.ang_avance= 180000+angulo;
		end
		case 7:
			father.ang_avance= 270000;
		end
		case 8:
			father.ang_avance= 0-angulo;
		end
	end		
	father.dx=father.avance*cos(father.ang_avance);
	father.dy=father.avance*sin(father.ang_avance)*-1;

	if(father.direccion == 3 or father.direccion == 7)
		father.dy = father.dy*0.6;
	end

	father.xi+=father.dx;
	father.yi+=father.dy;
end

function PLYR_salta_direccion(float velo_salto) // velo salto es porcentaje sobre avance normal=7 velo_salto[0.1,2]

private
	float angulo = 26565.05118;
end

begin
	//father.ang_avance=45000*(father.direccion-1);	
	switch (father.direccion)
		case 1:
			father.ang_avance= 0;
		end
		case 2:
			father.ang_avance= angulo;
		end
		case 3:
			father.ang_avance= 90000;
		end
		case 4:
			father.ang_avance= 180000-angulo;
		end
		case 5:
			father.ang_avance= 180000;
		end
		case 6:
			father.ang_avance= 180000+angulo;
		end
		case 7:
			father.ang_avance= 270000;
		end
		case 8:
			father.ang_avance= 0-angulo;
		end
	end		
	father.dx=(father.avance*velo_salto)*cos(father.ang_avance);
	father.dy=(father.avance*velo_salto)*sin(father.ang_avance)*-1;
	father.xi+=father.dx;
	father.yi+=father.dy;
end