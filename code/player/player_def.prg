type _animation
	int numFrames;
	int* aframe;
	int xCenter = 0; 
	int yCenter = 0;
end

type _animSet
	_animation Andar[7];
	_animation Idle[7];
end

Local
	int lastFrame = 0;	// Ultimo frame devielto para el proceso en la animacion

	int direccion=1;
	int estado=0; // 0=parado 1=moviendose 2=en salto
	int estado_salto; // 0=en suelo 1=subiendo 2=en aire 3=cayendo
	int fuerza_salto=10;
	float ang_avance=0;
	float avance=7;
	float dx, dy, dz;

	_animSet animaciones;
end
