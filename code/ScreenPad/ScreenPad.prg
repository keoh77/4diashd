/*
	Copyright: Francisco Manzano Magaña <keoh77@gmail.com>
	
	Fecha Inicio: 23 de Octubre de 2011

	| Modulo de Investigación, Desarrollo e Innovación |

	Consiste en una serie de funciones y procesos que uso con propositos 
	de investigación. Para aprender funciones que nunca he usado en BennuGD 
	y para hacer pruebas sobre sistemas que se me ocurran y hacer pruebas con 
	esa idea antes de meterla en ningún juego.

	Modulo Screen Pad: Modulo para crear un mando en pantalla para moviles y tabletas. Tiene pad y
	3 botones. Usa el modulo multitouch de Josebita. Se puede usar en PC pero con el raton, lo q no 
	es multitouch.

*/


import "mod_wm"
import "mod_proc"
import "mod_grproc"
import "mod_say"

import "mod_draw"
import "mod_map"
import "mod_file"
import "mod_mouse"
import "mod_text"
import "mod_math"

Const

End

Global
	//int res_escritorio_x, res_escritorio_y;
	//int res_x=1024, res_y=600;
	int fpg_spad, SP_angulo, SP_direccion, spad;
	byte screenPadActive=false;

End

Local

End

Process SP_ScreenPad()

Begin
	//get_desktop_size(&res_escritorio_x,&res_escritorio_y);
	//set_mode(res_x,res_y,32);
	//move_window((res_escritorio_x-res_x)/2,(res_escritorio_y-alto_pantalla)/2);

	//map_clear(0,BACKGROUND,rgb(127,125,120));

	SP_g_mouse();
	SP_crea_Spad();
	
	write_var(0,5,10,0,SP_angulo);
	write_var(0,5,20,0,SP_direccion);

End

function SP_crea_Spad()

begin

	fpg_spad=load_fpg("fpg/spad32.fpg");

	spad=SP_S_pad();

end

process SP_S_pad()

begin
	file=fpg_spad;
	graph=1;
	size=50;

	x=ancho_pantalla*0.13;
	y=alto_pantalla*0.81;
	
	loop
		if(abs(get_dist(type SP_g_mouse)<100))
			if(mouse.left)
				SP_direccion = SP_calcula_dir();
				else
				SP_direccion = 0;
			end
		end

		frame;
	end
end

process SP_g_mouse()

begin
	graph=png_load("images/flecha.png");
	loop
		x=mouse.x;
		y=mouse.y;
		frame;
	end
	
end

function SP_calcula_dir()

private
	float dx2,dy2,angulor,direccionn;
end

begin
	dx2=father.x-mouse.x;
	dy2=father.y-mouse.y;
	SP_angulo=fget_angle(father.x,father.y,mouse.x,mouse.y);
	switch(SP_angulo)
		case -89999 .. -67501:
			direccionn=7;
		end
		case -67500 .. -22501:
			direccionn=8;
		end	
		case -22500 .. 22500:
			direccionn=1;
		end
		case 22501 .. 67500:
			direccionn=2;
		end
		case 67501 .. 112500:
			direccionn=3;
		end
		case 112501 .. 157500:
			direccionn=4;
		end
		case 157501 .. 202500:
			direccionn=5;
		end
		case 202501 .. 247500:
			direccionn=6;
		end
		case 247501 .. 270000:
			direccionn=7;
		end
	end
	return direccionn;
end


