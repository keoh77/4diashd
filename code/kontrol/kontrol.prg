
/* ------ Función check_pad()	
	
	Devuelve 0 si no se usa la cruceta, y un numero para cada direccion del pad.
	 
	1 Este (E)
	2 NE
	3 Norte (N)
	4 NO
	5 Oeste (O)
	6 SO
	7 Sur (S)
	8 SE

*/	

function check_pad()

private
	int axis_up_down;
	int axis_left_right;
end

begin

	if(joy_activo==true)
		axis_up_down=joy_getaxis(0,1); //recoge el estado del eje arriba/abajo
		axis_left_right=joy_getaxis(0,0); //recoge el estado del eje izq/der
	
		if(axis_up_down==0 and axis_left_right==0) return 0; end
	
		if(axis_up_down==0 and axis_left_right<0) return 5; end
		if(axis_up_down==0 and axis_left_right>0) return 1; end
		if(axis_up_down>0 and axis_left_right==0) return 7; end	
		if(axis_up_down<0 and axis_left_right==0) return 3; end

		if(axis_up_down<0 and axis_left_right<0) return 4; end
		if(axis_up_down<0 and axis_left_right>0) return 2; end
		if(axis_up_down>0 and axis_left_right>0) return 8; end
		if(axis_up_down>0 and axis_left_right<0) return 6; end
	end

	if(screenPadActive == true)
		return SP_direccion;	
	end
	
	if(joy_activo==false)
		if(key(t_up) and key(t_der)) return 2; end
		if(key(t_up) and key(t_izq)) return 4; end
		if(key(t_down) and key(t_der)) return 8; end
		if(key(t_down) and key(t_izq)) return 6; end

		if(key(t_up)) return 3; end
		if(key(t_down)) return 7; end
		if(key(t_der)) return 1; end
		if(key(t_izq)) return 5; end
		
		if(key(t_up)==false and key(t_down)==false and key(t_der)==false and key(t_izq)==false) return 0; end
	end


end

function check_boton(int IDboton)  // Devuelve si se ta pulsado el boton correspondiente a IDboton, en mando o teclado

private
	int IDboton2;
end

begin
		if(IDboton==t_cancel) IDboton2=9; end
		if(IDboton==t_uso[1]) IDboton2=0; end
		if(IDboton==t_uso[2]) IDboton2=1; end
		if(IDboton==t_uso[3]) IDboton2=2; end
		if(IDboton==t_uso[4]) IDboton2=3; end
		if(IDboton==t_uso[5]) IDboton2=4; end
		if(IDboton==t_uso[6]) IDboton2=5; end
		if(IDboton==t_uso[7]) IDboton2=6; end
		if(IDboton==t_uso[8]) IDboton2=7; end
		if(IDboton==t_uso[9]) IDboton2=8; end


		if(joy_activo==true)
			return joy_getbutton(0,IDboton2);
		end
		if(joy_activo==false)
			return key(IDboton);
		end
	

end